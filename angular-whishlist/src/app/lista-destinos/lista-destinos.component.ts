import { Component, OnInit } from '@angular/core';
import { DestinoViaje } from '../models/destino-Viaje.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.sass']
})
export class ListaDestinosComponent implements OnInit {
  destinos: DestinoViaje[];
  listaDestinos:string[];
  aux:DestinoViaje;
  constructor() { 
    this.destinos =[];
    this.listaDestinos = [];
    this.aux = new DestinoViaje("","");
  }

  ngOnInit(): void {
  }
  guardar(nombre:string, url: string):boolean{
    this.aux=new DestinoViaje(nombre, url);
    this.destinos.push(this.aux);
    this.listaDestinos.push(this.aux.nombre);
    return false;
  }

  elegido(d:DestinoViaje){
    this.destinos.forEach(function(x){
      x.setSelected(false);
    });
    d.setSelected(true);
  }
}
